<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>
	<?php  
		$footer_cta = get_field('footer_cta', 'options');
		$footer_cta_bg = get_field('footer_cta_background', 'options');
	?>
	<?php if ($footer_cta && $footer_cta_bg && !is_page('get-involved')): ?>
		<div class="footer-cta">
			<div class="footer-cta-bg">
				<img src="<?php echo $footer_cta_bg['url']; ?>" alt="<?php echo $footer_cta_bg['alt']; ?>">
			</div>
			<div class="footer-cta-content container">
				<?php echo $footer_cta; ?>
			</div>
		</div>
	<?php endif ?>
	<footer id="site-footer">
		
		<div class="pt-5 pb-3">
			<div id="site-footer-main" class="clearfix pb-3 container justify-content-center align-items-start flex-wrap">
				<div class="row">
					<div class="site-footer-alpha col-md-4 col-lg-3 text-center text-md-left"><?php dynamic_sidebar('footer-alpha'); ?></div>
					<div class="site-footer-bravo col-md-4 col-lg-3 text-center text-md-left"><?php dynamic_sidebar('footer-bravo'); ?></div>
					<div class="site-footer-charlie col-md-4 col-lg-2 text-center text-md-left"><?php dynamic_sidebar('footer-charlie'); ?></div>
					<div class="site-footer-delta col-md-4 col-lg-2 text-center text-md-left"><?php dynamic_sidebar('footer-delta'); ?></div>
					<div class="site-footer-echo col-md-4 col-lg-2 text-center text-md-left"><?php dynamic_sidebar('footer-echo'); ?></div>
				</div>
			</div>

			<?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
				<div id="site-legal" class="container pt-1">
					<div class="d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
						<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?> | <a href="/privacy-policy">Privacy Policy</a></div>
						<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
					</div>
				</div>
			<?php endif; ?>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action('document_end'); ?>
