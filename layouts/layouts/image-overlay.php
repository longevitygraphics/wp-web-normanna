<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part( '/layouts/partials/block-settings-start' );

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if ( $container == 'container-wide' ) {
	echo 'no-gutters';
} ?> row <?php the_sub_field( 'align_items_vertical' ); ?> <?php the_sub_field( 'align_items_horizontal' ); ?>">
	<div class="col-12">
		<?php if ( have_rows( "content" ) ): ?>
			<div class="d-flex flex-wrap image-overlay-wrapper">
				<?php while ( have_rows( "content" ) ): the_row(); ?>
					<?php
					$image  = get_sub_field( "image" );
					$info   = get_sub_field( "info" );
					$title  = get_sub_field( "title" );
					$col_md = get_sub_field( "col_medium_width" );
					$col_lg = get_sub_field( "col_large_width" );
					//pr( $image );
					$image_srcset = $image['sizes']['medium'] .' '.$image['sizes']['medium-width'].'w';
					$image_sizes = '(min-width: 992px) 260px, 100vw';
					?>
					<div class="pl-0 pr-0 pr-md-3 col-md-<?php echo $col_md; ?> col-lg-<?php echo $col_lg; ?>">
						<div class="image-overlay">
							<div class="image-overlay-top">
								<div class="image-overlay-img">
									<img
										class="img-full"
										src="<?php echo $image['url']; ?>"
										srcset="<?php echo $image_srcset; ?>"
										sizes="<?php echo $image_sizes; ?>"
										alt="<?php echo $image['alt']; ?>"></div>
								<div class="image-overlay-content"><?php echo $info; ?></div>
							</div>
							<div class="py-2 image-overlay-bottom">
								<?php echo $title; ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part( '/layouts/partials/block-settings-end' );

?>
