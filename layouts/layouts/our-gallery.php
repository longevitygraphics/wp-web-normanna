<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part( '/layouts/partials/block-settings-start' );

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if ( $container == 'container-wide' ) {
	echo 'no-gutters';
} ?> row <?php the_sub_field( 'align_items_vertical' ); ?> <?php the_sub_field( 'align_items_horizontal' ); ?>">
    <div class="col-12">
		<?php if ( have_rows( "filters" ) ): ?>
            <div class="tab-filter-a gallery-filters mb-4">
				<?php $i = 0; ?>
				<?php while ( have_rows( "filters" ) ) : the_row(); ?>
					<?php
					$i ++;
					?>
					<?php $filter = get_sub_field( "filter" ); ?>
                    <a
                            class="text-capitalize filter ml-2"
                            data-tab="gallery-filter<?php echo $i; ?>"
                            href="javascript:;"
                            slug="<?php echo $filter; ?>"
                    ><?php echo $filter; ?></a>
				<?php endwhile; ?>
            </div>
		<?php endif; ?>
        <!-- end of filters -->

		<?php if ( have_rows( "gallery" ) ): ?>
            <div class="d-flex flex-wrap gallery">
                <div class="grid-sizer"></div>
				<?php while ( have_rows( "gallery" ) ) : the_row(); ?>
					<?php
					$image = get_sub_field( "image" );
					$types = get_sub_field( "types" );
					?>
                    <div class="gallery-image all <?php if ( $types ) {
						foreach ( $types as $type ) {
							echo $type['value'] . ' ';
						}
					} ?>">
                        <div>
                            <div class="image">
                                <div class="iso-img-wrapper">
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
            </div>
		<?php endif; ?>

    </div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part( '/layouts/partials/block-settings-end' );

?>

<script>
    (function ($) {

        // var default_hash = 'all';

        $(window).on('load', function () {
            var content = $('.gallery');
            var $mas = content.masonry({
                // options
                columnWidth: '.grid-sizer',
                itemSelector: '.gallery-image',
            });

            $(window).on('hashchange', function () {
                var urlHash = 'all';
                if (location.hash) {
                    urlHash = location.hash.substring(1);
                }
                // if(location.hash == '' || !$('.filter[slug="'+ location.hash.substring(1) +'"]')[0]){
                //     location.hash = default_hash;
                // }

                $('.filter[slug="' + urlHash + '"]').addClass('active').siblings().removeClass('active');
                //content.find('>div').fadeOut(100, function(){
                //show_animate(content.find('.'+location.hash.substring(1)));
                //});
                content.find('>div').removeClass('gallery-image');
                content.find('.' + urlHash).addClass('gallery-image');
                $mas.masonry();

            }).trigger('hashchange');
        });

        $(document).ready(function () {

            var filter = $('.tab-filter-a');
            var content = $('.gallery');
            filter.find('.filter').on('click', function () {
                location.hash = $(this).attr('slug');
            });

        });

    }(jQuery));
</script>
