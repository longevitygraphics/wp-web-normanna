<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!---------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php if (have_rows("content")): ?>
				<div class="d-flex flex-wrap card-overlay-component-wrapper">
				<?php while(have_rows("content")): the_row(); ?>
				<?php
					$image = get_sub_field("image");
					$info = get_sub_field("info");
					$title = get_sub_field("title");
					$col_md = get_sub_field("col_medium_width");
					$col_lg = get_sub_field("col_large_width");
					$overlay_bg = get_sub_field("overlay_color");
				?>
				<div class="px-0 p-md-3 col-md-<?php echo $col_md; ?> col-lg-<?php echo $col_lg; ?>">
					<div class="card">
						<div class="card-top">
							<div class="card-img"><img class="img-full" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
							<div class="card-overlay-content transparent-bg-<?php echo $overlay_bg; ?>"><?php echo $info; ?></div>
						</div>
						<div class="text-center py-2">
							<h4 class="mb-0"><?php echo $title; ?></h4>
							<!-- <div class="card-overlay-content card-overlay-content-mobile"><?php echo $info; ?></div> -->
						</div>
					</div>
				</div>
				<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

<!---------------------------------------------------------------------------------------------------------------------------------->

<?php

	get_template_part('/layouts/partials/block-settings-end');

?>
