<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<div class="two-column-history-wrapper d-flex">
				<?php $image = get_sub_field("image"); ?>
				<?php $title = get_sub_field("title"); ?>
				<div class="two-column-procedure-image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
				</div>
				<?php if (have_rows("history")): ?>
					<div class="d-flex flex-column justify-content-center bg-white py-4 py-md-3">
						<div class="history-title"><?php echo $title; ?></div>
						<div class="histories mb-3">
						<?php while(have_rows("history")): the_row(); ?>
							<?php $slide = get_sub_field("slide"); ?>
							<div class="history-slide">
								<?php echo $slide; ?>
							</div>
						<?php endwhile ?>
						</div>
						<div class="slider-nav">
							<div class="prev-slide slick-arrow"><i class="fas fa-arrow-left"></i></div>
		    				<div class="next-slide slick-arrow"><i class="fas fa-arrow-right"></i></div>
						</div>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
	
<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>