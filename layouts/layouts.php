<?php

	switch ( get_row_layout()) {
		case 'image_overlay':
			get_template_part('/layouts/layouts/image-overlay');
		break;
		case 'card_overlay':
			get_template_part('/layouts/layouts/card-overlay');
		break;
		case 'our_gallery':
			get_template_part('/layouts/layouts/our-gallery');
		break;
		case 'two_column_history':
			get_template_part('/layouts/layouts/two-column-history');
		break;
	}

?>