<?php
function lg_widgets_init() {
	if (function_exists('register_sidebar')) {
		$sidebar1 = array(
			'name'          => esc_html__( 'Footer Delta', '_s' ),
			'id'            => 'footer-delta',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		);
		$sidebar2 = array(
			'name'          => esc_html__( 'Footer Echo', '_s' ),
			'id'            => 'footer-echo',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		);
		register_sidebar($sidebar1);
    	register_sidebar($sidebar2);
	}

}
add_action( 'wp_loaded', 'lg_widgets_init' );