<?php

function main_nav_items( $items, $args ) {
	if ( $args->menu and is_object( $args->menu ) and $args->menu->slug == 'main-menu' ) {
		$items .= '<li class="align-self-lg-center nav-search"><a href="#" class="nav-link"><i class="fas fa-search"></i></a><form action="/" id="header-search-form" method="get"><input type="text" name="s" id="s" placeholder="Search"><button type="submit"><i class="fas fa-search"></i></button></form></li>';
		$items .= '<li class="nav-book menu-item menu-item-type-custom menu-item-object-custom nav-item d-none d-xl-inline-block"><a class="nav-link btn btn-primary text-white" href="/fundraising-and-donations/">Donate</a></li>';
	}

	return $items;
}

add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );
add_action( 'wp_content_top', 'featured_banner_top', 1 ); // ('wp_content_top', defined function name, order)

function featured_banner_top() {
	ob_start();
	get_template_part( '/templates/template-parts/page/top-banner' );
	echo ob_get_clean();
}


/*
 * Adds start time to event titles in Month view
 */
// function tribe_add_start_time_to_event_title ( $post_title, $post_id ) {

// 	if ( !tribe_is_event($post_id) ) return $post_title;
// 	// Checks if it is the month view, modify this line to apply to more views
// 	if ( !tribe_is_month() ) return $post_title;

// 	$event_start_time = tribe_get_start_time( $post_id );

// 	if ( !empty( $event_start_time ) ) {
// 		$post_title = $event_start_time . ' - ' . $post_title;
// 	}

// 	return $post_title;
// }
// add_filter( 'the_title', 'tribe_add_start_time_to_event_title', 100, 2 );

/*
* numeric pagination
*/
function numeric_posts_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="pagination"><ul>' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link("<<") );

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link(">>") );

    echo '</ul></div>' . "\n";

}


// remove dashicons in frontend to non-admin
function lg_dequeue_dashicon() {
	if (current_user_can( 'update_core' )) {
		return;
	}
	wp_deregister_style('dashicons');
}
add_action( 'wp_enqueue_scripts', 'lg_dequeue_dashicon' );
