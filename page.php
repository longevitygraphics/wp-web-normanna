<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>
<?php


?>
    <main>
		<?php
		global $wp_query, $post;
		//pr($post);
		//echo get_the_ID();
		//pr($wp_query->query_vars);
		if ( $wp_query->query_vars['post_type'] == 'tribe_events' ) : ?>
            <div class="container">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					the_content();

				endwhile; // End of the loop.
				?>
            </div>
			<?php
			if ( $wp_query->query_vars['post_type'] == 'tribe_events' && $wp_query->query_vars['eventDisplay'] == 'month' ) {
				$post = get_page_by_title( 'events' );
			}
			?>
		<?php endif; ?>

		<?php flexible_layout(); ?>

    </main>

<?php get_footer(); ?>