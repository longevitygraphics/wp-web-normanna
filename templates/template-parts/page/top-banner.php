<?php
global $wp_query, $post;
if ( $wp_query->query_vars['post_type'] == 'tribe_events' && $wp_query->query_vars['eventDisplay'] == 'month' ) {
	$post = get_page_by_title( 'events' );
}

$top_banner_images  = get_field( "top_banner" )['gallery'];
$gallery            = $top_banner_images;
$text_overlay       = get_field( "top_banner_overlay" );
$is_home_top_banner = is_front_page() ? 'home-top-banner' : '';
?>
<?php if ( $top_banner_images && ! is_search() ) : ?>
    <div class="top-banner <?php echo $is_home_top_banner ?>">
		<?php include( locate_template( '/layouts/components/gallery.php' ) ); ?>
		<?php if ( $text_overlay ) : ?>
			<?php
			echo "<div class='top-banner-overlay animated fadeInUp'>";
			echo "<div class='container'>";
			echo "<div class='banner-text'>";
			echo $text_overlay;
			echo "</div>"; //end of banner text
			echo "</div>";
			echo "</div>"; // end of top banner overlay
			?>
		<?php endif; ?>

    </div>
<?php endif ?>