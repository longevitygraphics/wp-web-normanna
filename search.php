<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

    <?php
        $banner = get_option('lg_option_blog_archive_banner_image');
        $banner_height = get_option('lg_option_blog_archive_banner_height') ? get_option('lg_option_blog_archive_banner_height') : '400px';
        $blog_style = get_option('lg_option_blog_style') ? get_option('lg_option_blog_style') : 'list';
    ?>

    <main class="blog pb-5">
        <?php if($banner): ?>
            <div class="blog-banner" style="height: <?php echo $banner_height; ?>">
                <img src="<?php echo $banner; ?>">
            </div>
        <?php endif; ?>

        <div class="container">
    <?php if ( have_posts() ) : ?>
    <div class="blog_list_small py-3 mt-5">
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="row no-gutters pb-3">
        <?php if(has_post_thumbnail()): ?>
            <div class="col-md-5">
                <img src="<?php echo get_the_post_thumbnail_url(); ?>">
            </div>
            <div class="col-md-7 px-md-5 py-4 py-md-0">
                <header>
                    <h2><a class="text-dark" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                </header>
                <div class="article-content">
                    <p><?php the_excerpt(); ?></p>
                    <a class="d-inline-block mt-4 text-secondary arrow-link link--arrowed" href="<?php echo get_permalink(); ?>">CONTINUE READING<?php include 'arrow.svg'; ?></i></a>
                </div>
            </div>
        <?php else: ?>
            <div class="col-12">
                <header>
                    <h2><a class="text-dark" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                </header>
                <div class="article-content">
                    <p><?php the_excerpt(); ?></p>
                </div>
                <a class="d-inline-block mt-4 text-secondary arrow-link link--arrowed" href="<?php echo get_permalink(); ?>">CONTINUE READING<?php include 'arrow.svg'; ?></i></a>
            </div>
        <?php endif; ?>

        <?php if (($wp_query->current_post +1) != ($wp_query->post_count)): ?>
            <hr class="lg d-none d-md-block">
        <?php endif; ?>
        </div>
        <?php endwhile; ?>
        </div>
        <?php else: ?>
            <h2 class="text-center pt-5">Sorry, but nothing matched your search criteria. Please try again with some different keywords</h2>
    <?php endif ?>
            <?php numeric_posts_nav(); ?>
        </div>
    </main>

<?php get_footer(); ?>