// Window Scroll Handler

(function($) {
	var position = document.documentElement.scrollTop;
    $(window).on('scroll', function(){
		var scroll = document.documentElement.scrollTop;
		if (scroll > position) {
			$('.header-main').addClass('small-header');
			if ($(window).width() > 992) {
				$('.main-navigation').slideUp();
			}
		} else {
			$('.header-main').removeClass('small-header');
			if ($(window).width() > 992) {
				$('.main-navigation').slideDown();
			}
		}
		position = scroll;
    })

}(jQuery));