// Windows Ready Handler
 	
(function($) {
    $(document).ready(function(){
		$('.nav-search > .nav-link').click(function () {
            $(this).next("#header-search-form").toggleClass("show-search-form");
        });
        //show overlay on click (for mobile)
        $(".image-overlay-wrapper > div").click(function(){
            $(this).children(".image-overlay-content").toggleClass('show-overlay-content');
            console.log("click");
        })
        //two column history
        $('.histories').slick({
            dots: false,
            infinite: true,
            fade: false,
            slidesToShow: 1,
            arrow: true,
            appendArrows: '.slider-nav',
            prevArrow: $('.prev-slide'),
            nextArrow: $('.next-slide'),
            autoplay: true,
            autoplaySpeed: 15000,
            adaptiveHeight: true
        });

        // smooth scroll to id
        $('a[href*="#"]').on('click', function(e) {
          e.preventDefault()

          $('html, body').animate(
            {
              scrollTop: $($(this).attr('href')).offset().top,
            },
            500,
            'linear'
          )
        });

        /******* slide to this section ******/
        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }
        
        var para = getUrlVars()["slideto"];
            var element = $('#'+para);
            var settings = {
                duration: 2 * 1000,
                offset: -80
            };
            if(para && element[0]){
                var scrollTo = element;
                KCollection.headerScrollTo(scrollTo, settings, function(){
                    console.log('here now');
                });
            }  

    });
}(jQuery));